from django.shortcuts import render

#get access to model
from .models import Job
# Create your views here.
def home(request):
    #pull all Job
    jobs=Job.objects
    return render(request,'jobs/home.html',{'jobs':jobs})
